Ansible Role: puppet-control
=========

Enable, disable or run puppet on your server.

Requirements
------------

You need a working installation of puppet on the hosts your playbook is manipulating.

Role Variables
--------------

puppet_path: /opt/puppetlabs/bin  
puppet_disable_message:
  - This variable can be used with --extra-vars when running ansible-playbook  
    `
    ansible-playbook disable_puppet.yml -e "puppet_disable_message='my disable message'"
    `


Dependencies
------------

None.

Example Playbook
----------------

    - hosts: servers
      tasks:
        - include_role:
            name: puppet-control
            tasks_from: enable

License
-------

MIT

